
require('console-stamp')(console, { pattern: 'dd/mm/yyyy HH:MM:ss.l' });
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const upload = require('express-fileupload')
const errorHandler = require('errorhandler')
const routes = require('./routes')
const dotenv = require('dotenv')
const { requestio } = require('./middlewares/beforeRequest')

var logger = require('morgan');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.config({ path: '.env' })

const app = express()

app.set('host', process.env.NODEJS_IP || '0.0.0.0');
app.set('port', process.env.PORT || 3004);

app.use(cors());
app.use(bodyParser.raw({ type: 'application/pgp-encrypted' }))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(upload())
app.use(requestio)
app.use(logger('dev'));

app.use(`/health`, (req, res) => res.send())
app.use(`${process.env.API_PREFIX || ''}/`, routes)

if (['development', 'local'].includes(process.env.NODE_ENV)) {
  app.use(errorHandler());
} else {
  app.use((err, req, res, next) => {
    console.error(err);
    res.status(500).send('Server Error');
  });
}

app.listen(app.get('port'), () => console.log(`Example app listening on port ${app.get('port')}!`))