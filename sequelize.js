var Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const dotenv = require('dotenv')
dotenv.config({ path: '.env' })

const db = {};
var sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  define: {
    timestamps: true,
    freezeTableName: true
  },
  logging: console.log,
  logQueryParameters: true
});

fs
  .readdirSync(path.join(__dirname, 'models'))
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, 'models', file));
    // console.log(model);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

sequelize.sync({ force: false })
  .then(() => {
    console.log(`Database & tables created here!`)
  })

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
