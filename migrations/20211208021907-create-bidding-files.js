'use strict';
module.exports = {
  up: async (queryInterface, type) => {
    await queryInterface.createTable('bidding_files', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: type.INTEGER 
      },
      status: {
          type: type.STRING(2),
          allowNull: true,
          default: 'Y',
          comment: 'Trạng thái của tệp',
      },
      url: {
        type: type.STRING,
        comment: 'Đường dẫn ảnh',
      },
      filename: {
        type: type.STRING,
        allowNull: true,
        comment: 'Tên ảnh',
      },
      thumbnailUrl: {
        type: type.STRING,
        allowNull: true,
        comment: 'thumbnail',
      },
      size: {
        type: type.INTEGER,
        allowNull: true,
        comment: 'kích thước file',
      },
      contentType: {
        type: type.STRING,
        allowNull: true,
        comment: 'Định dạng file',
      },
      registerId: {
          type: type.UUID,
          allowNull: false,
          comment: 'mã hồ sơ đăng kí',
      },
      itemName: {
          type: type.INTEGER,
          allowNull: true,
          comment: 'Loại file: Đính kèm hay mẫu giấy ủy quyền: 1, 2',
      },
      createdAt: {
        allowNull: false,
        type: type.DATE
      },
      updatedAt: {
        allowNull: false,
        type: type.DATE
      }
    });
  },
  down: async (queryInterface, type) => {
    await queryInterface.dropTable('bidding_files');
  }
};