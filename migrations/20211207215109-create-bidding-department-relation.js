'use strict';
module.exports = {
  up: async (queryInterface, type) => {
    await queryInterface.createTable('bidding_department_relations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: type.INTEGER
      },
      unitId: {
          type: type.UUID,
          allowNull: false,
          comment: 'Mã phòng ban NV liên quan',
      },
      registerId: {
          type: type.UUID,
          allowNull: false,
          comment: 'Mã hồ sơ đăng kí',
      },
      createdAt: {
        allowNull: false,
        type: type.DATE
      },
      updatedAt: {
        allowNull: false,
        type: type.DATE
      }
    });
  },
  down: async (queryInterface, type) => {
    await queryInterface.dropTable('bidding_department_relations');
  }
};