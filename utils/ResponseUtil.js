const errorCode = {
    '0000': 'Thành công',
    1000: 'Lỗi không xác định',
    1001: 'Dữ liệu đầu vào không hợp lệ',
    1002: 'Tham số không hợp lệ',
    1003: 'Thiếu dữ liệu đầu vào',
    1004: 'Thiếu tham số đầu vào',
    1005: 'Yêu cầu không hợp lệ',
    1006: 'Thông tin người mua không hợp lệ',
    1007: 'Thông tin người được bảo hiểm không hợp lệ',
    1008: 'Thông tin xe không hợp lệ',
    1009: 'Thông tin người nhận ấn chỉ không hợp lệ',
    1010: 'Phí bảo hiểm không được bỏ trống',
    1011: 'Phí bảo hiểm không hợp lệ',
    1012: 'Thời gian không hợp lệ',
    1013: 'Thời gian không đúng định dạng',
    1014: 'Số điện thoại không hợp lệ',
    1015: 'Địa chỉ email không hợp lệ',
    1016: 'CMND/ CCCD/ Hộ chiếu không hợp lệ',
    1017: 'Độ tuổi của người được bảo hiểm không hợp lệ',
    1018: 'Ngày bắt đầu hiệu lực không hợp lệ',
    1019: 'Ngày kết thúc hiệu lực không hợp lệ',
    1020: 'Số tháng hiệu lực không hợp lệ',
    1021: 'Gói bảo hiểm không hợp lệ',
    1022: 'Gói bảo hiểm không tồn tại',
    1023: 'Họ và tên không hợp lệ',
    1024: 'Email không hợp lệ',
    1025: 'Mã số thuế không hợp lệ',
    1026: 'Biển số xe không hợp lệ',
    1027: 'Mã hợp đồng không hợp lệ',
    1028: 'Số IMEI/serial không đúng định dạng',
    1050: 'Thiếu tham số đầu vào: Số điện thoại',
    2001: 'Hợp đồng không tồn tại',
    2002: 'Hợp đồng đang xử lý',
    3001: 'Gói internet không hợp lệ'
}

class ResponseUtil {

    constructor() {
        this.statusCode = null;
        this.data = null;
        this.message = null;
    }

    set(statusCode, data, options) {
        this.statusCode = statusCode;
        this.message = errorCode[statusCode] || 'Lỗi không xác định';
        this.data = data;
        this.options = options;
    }

    send(res) {
        let result = {
            code: this.statusCode,
            message: this.message,
            ...this.options,
        };

        if (this.statusCode === '0000') {
            result.data = this.data
        }

        return res.status(200).json(result);
    }

}

module.exports = ResponseUtil