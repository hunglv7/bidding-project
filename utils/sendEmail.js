const SendEmail = require('../plugins/SendEmail')

var sendEmailPlg

module.exports = {
  sendCreateContractSuccess: function ({ subject = '', email = '', html = '' }) {
    if (!sendEmailPlg) {
      sendEmailPlg = (new SendEmail())
    }
    sendEmailPlg.send({ email, html, subject })
  }
}