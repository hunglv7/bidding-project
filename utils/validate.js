const moment = require('moment')

function isLength(string, options) {
  let min;
  let max;
  if (typeof (options) === 'object') {
    min = options.min || 0;
    max = options.max;
  } else { // backwards compatibility: isLength(str, min [, max])
    min = arguments[1] || 0;
    max = arguments[2];
  }
  let str = string ? string.toString() : ''
  const surrogatePairs = str.match(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g) || [];
  const len = str.length - surrogatePairs.length;
  return len >= min && (typeof max === 'undefined' || len <= max);
}

function isEmail(email) {
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email.toLowerCase());
}

function isPhone(str, onlyVN = false) {
  if (!str)
    return false
  if (typeof str !== 'string' && typeof str !== 'number')
    return false
  str.toString()
  const regex1 = /^0[0-9]{9}$/i
  const regex2 = /^84[0-9]{9}$/i
  const regex3 = /^(19|18)00[0-9]{4}$/i
  const regex = /^\+(?:[0-9]?){6,14}[0-9]$/
  if (onlyVN) {
    return regex1.test(str) || regex2.test(str) || regex3.test(str)
  }
  return regex1.test(str) || regex2.test(str) || regex3.test(str) || regex.test(str)
}

function isPhoneStartWithZero(str) {
  if (!str)
    return false
  if (typeof str !== 'string' && typeof str !== 'number')
    return false
  str.toString()
  const regex = /^0[0-9]{9}$/i

  return regex.test(str)
}

function isAlpha(str) {
  const regex = /^[a-zàáâãèéêìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểếễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹý\s]+$/i
  return regex.test(str);
}

function isAlphaNumeric(str) {
  const regex = /^[a-z0-9àáâãèéêìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểếễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹý\s]+$/i
  return regex.test(str);
}

function isAlphaLatin(str) {
  const regex = /^[a-z\s]+$/i
  return regex.test(str);
}

function isAlphaLatinNumeric(str) {
  const regex = /^[a-z0-9]+$/i
  return regex.test(str);
}

function isNumber(str) {
  const regex = /^\d*\.?\d*$/
  return regex.test(str);
}

function isUuid(uuid) {
  return /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(uuid)
}

function isDateTime(dateTime) {
  return /^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}?(.[0-9]{3}Z)?([+][0-9]{2}:[0-9]{2})?(Z)?$/.test(dateTime)
}

function isDate(date) {
  return /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(date)
}

const greater18y = (datetime) => {
  try {
    return moment(datetime).add(18, 'years').add(7, 'hours').startOf('day') < moment().add(7, 'hours').startOf('day')
  } catch (error) {
    console.log(error);
  }
}

const greater70y = (datetime) => {
  try {
    return moment(datetime).add(70, 'years').add(7, 'hours').startOf('day') < moment().add(7, 'hours').startOf('day')
  } catch (error) {
    console.log(error);
  }
}

const greater15d = (datetime) => {
  try {
    return moment(datetime).add(15, 'days').add(7, 'hours').startOf('day') < moment().add(7, 'hours').startOf('day')
  } catch (error) {
    console.log(error);
  }
}

const isUUID = (str) => {
  const regex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
  return regex.test(str)
}

function removeAscent(str) {
  if (str === null || str === undefined) return str;
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  return str;
}

const isFullName = (str) => {
  const regex = /^[a-zàáâãèéêìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểếễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹý'\s]+$/i
  return regex.test(str);
}

const isCardId = (str, isChildren) => {
  const regex = /^[a-zA-Z0-9]{8,12}$/i
  if (isChildren) {
    return regex.test(str) || (/^[a-zA-Z0-9]{8,12}\/[0-9]{1,2}$/i).test(str)
  }
  return regex.test(str)
}

const isLotteCardId = (str) => {
  const regex = /^[a-zA-Z0-9]{7,14}$/i

  return regex.test(str)
}

const isMotorbikeLicenseNumber = (value) => {
  if (!value) return false
  // const regex = /^[0-9]{2}[a-zA-ZĐ]{1,2}[0-9]{5,6}$/
  const regex = /^[a-zA-Z0-9]{1,20}$/i
  return regex.test(value)
}

const isCarLicenseNumber = (value) => {
  if (!value) return false
  // const regex1 = /^[0-9]{2,}[a-zA-Z]{1,}[0-9]{4,6}$/
  // const regex2 = /^[0-9]{5,}[a-zA-Z]{2,}[0-9]{2,3}$/
  const regex = /^[a-zA-Z0-9]{1,20}$/i
  return regex.test(value)
}

const isTaxCode = value => {
  if (!value) return false
  const regex1 = /^[0-9]{10}$/
  const regex2 = /^[0-9]{10}-[0-9]{3}$/
  return regex1.test(value) || regex2.test(value)
}

const isTicketNumber = (value) => {
  if (!value) return false
  const regex = /^738[0-9]{10}$/
  return regex.test(value)
}

function isValidContractCode(str) {
	return isContractCode(str) || isAmendmentContractCode(str);
}

function isContractCode(str) {
	if (typeof str === 'string') {
		return /^[0-9A-Z]+\.[0-9A-Z]+\.[0-9A-Z]+\.[0-9]{11}$/i.test(str.trim())
	}
	return false
}

// Check mã HD sửa đổi bổ sung
function isAmendmentContractCode(str) {
	if (typeof str === 'string') {
		return /^[0-9]{2}SDBS[0-9A-Z]+\.[0-9A-Z]+\.[0-9A-Z]+\.[0-9]{11}$/i.test(str)
	}
	return false
}

function validBirthday(str, format = 'DD/MM/YYYY') {
  return moment(str, format, true).isValid() && moment().diff(moment(str, format, true), 'days') > 0
}

const isLicenseNumber = (value) => {
  if (!value) return false
  const regex = /^[a-zA-Z0-9-.]{1,20}$/i
  return regex.test(value)
}

module.exports = {
  isLength,
  isEmail,
  isPhone,
  isAlpha,
  isAlphaNumeric,
  isNumber,
  isUuid,
  isDateTime,
  isDate,
  isAlphaLatinNumeric,
  greater18y,
  greater70y,
  greater15d,
  isUUID,
  isFullName,
  isCardId,
  isMotorbikeLicenseNumber,
  isCarLicenseNumber,
  isTaxCode,
  isAlphaLatin,
  isTicketNumber,
  isValidContractCode,
  isContractCode,
  isAmendmentContractCode,
  validBirthday,
  isPhoneStartWithZero,
  isLotteCardId,
  isLicenseNumber,
}