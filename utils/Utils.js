const { isNumber } = require("lodash");
const moment = require('moment');
const { isPhone } = require("./validate");

const formatErrors = (error) => {
	let str = error || 'error default msg';
	if (typeof str === 'string') {
		return str
	}
	if (error && error.errors) {
		if (error.errors.length > 0) {
			str = ''
			error.errors.forEach((err) => {
				str += `${err.message} | `
			})
		}
		str = str.replace(/\s\|\s$/, '')
	}
	if (typeof str === 'object') {
		return str
	}
	return new Error(str)
}

const errorStringfy = (error) => {
	if (typeof error === 'string') {
		return error
	}
	if (error && error.errors) {
		if (error.errors.length > 0) {
			let str = ''
			error.errors.forEach((err) => {
				str += `${err.message} | `
			})
		}
		str = str.replace(/\s\|\s$/, '')
		return str
	}
	if (typeof error === 'object') {
		if (error.message) {
			return error.message
		}
	}
	return JSON.stringify(error)
}

const formatVND = (amount, suffix = '.') => {
	if (Number(amount)) {
		var re = '\\d(?=(\\d{3})+$)';
		return amount.toString().replace(new RegExp(re, 'g'), `$&${suffix}`);
	}
	return 0
}

const addPaging = (
	req,
	condition,
	defaultSort = "id",
	defaultOrder = "ASC",
	defaultStart = 0,
	defaultLimit = 10,
) => {
	var conditionGet = condition;
	var { sort, start, order, limit } = req.query;
	sort = validateSort(sort, defaultSort);
	order = validateOrder(order, defaultOrder);
	start = validateNumber(start, defaultStart);
	limit = validateNumber(limit, defaultLimit);

	conditionGet.order = [[sort, order]];
	conditionGet.offset = start;
	conditionGet.limit = limit;
	return conditionGet;
}

function removeMark(str) {
	if (typeof str === 'string') {
		str = str.toLowerCase();
		str = str.replace(/ậ|ẩ|ấ|ạ|à|á|ả|ã|a|ầ|ặ|ắ|ằ|à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/gi, "a");
		str = str.replace(/ể|ệ|ế|ề|ẽ|ẹ|è|é|ẻ|e|ễ|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/gi, "e");
		str = str.replace(/ị|í|ĩ|ì|i|ì|í|ị|ỉ|ĩ/gi, "i");
		str = str.replace(/õ|ộ|ớ|ỗ|ợ|ở|ồ|ố|ờ|ọ|ò|õ|ó|õ|ỏ|o|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|ø/gi, "o");
		str = str.replace(/ử|ữ|ự|ứ|ú|ụ|ù|ũ|ú|ủ|u|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/gi, "u");
		str = str.replace(/ỳ|ỹ|ý|y|ỳ|ý|ỵ|ỷ|ỹ/gi, "y");
		str = str.replace(/đ/gi, "d");
	}
	return str;
}

class Util {
	constructor() {
		this.statusCode = null;
		this.type = null;
		this.data = null;
		this.message = null;
		this.totalLength = null;
		this.errors = null
		this.extraData = null;
	}

	setSuccess(statusCode, message, data = {}) {
		this.statusCode = statusCode;
		this.message = message;

		if (data.links) {
			this.links = data.links
		}
		else {
			this.links = null
		}
		if (isNumber(data.totalLength)) {
			this.totalLength = data.totalLength
		}
		else {
			this.totalLength = null
		}
		this.extraData = null;
		if (data.extraData) {
			this.extraData = data.extraData;
		}
		this.data = data.data;
		this.type = 'success';
	}

	setError(statusCode, err, errors) {
		this.statusCode = statusCode;
		this.message = typeof err === 'object' ? err.message : err;
		this.type = 'error';
		this.errors = errors
	}

	send(res) {

		let result = {
			status: this.type,
			message: this.message,
			data: this.data,
		};

		if (this.links) {
			result.links = this.links
		}
		if (isNumber(this.totalLength)) {
			result.totalLength = this.totalLength
		}
		if (this.extraData) {
			result.extraData = this.extraData;
		}

		if (this.type === 'success') {
			return res.status(this.statusCode).json(result);
		}

		return res.status(this.statusCode).json({
			status: this.type,
			message: this.message,
			errors: this.errors
		});
	}
}

var subTimestamp = function (time1, time2, unit = 'minute', exactly = false) {
	try {
		return moment(time1).diff(moment(time2), unit, exactly)
	} catch (error) {
		console.log(error);
		throw error
	}
}

function getYearsOld(birthday, format = 'DD/MM/YYYY') {
	const now = moment().endOf('day');
	const birthdayTime = moment(birthday, format).endOf('day');
	const currentYear = now.year();
	const yearOfBirthday = birthdayTime.year();

	let yearOld = currentYear - yearOfBirthday;
	if (now.month() > birthdayTime.month()) {
		return yearOld;
	}
	if (now.month() < birthdayTime.month()) {
		yearOld = (yearOld > 0) ? yearOld - 1 : 0;
		return yearOld;
	}

	if (now.date() >= birthdayTime.date()) {
		return yearOld;
	}

	yearOld = (yearOld > 0) ? yearOld - 1 : 0;
	return yearOld;
}

function convertTitleCase(str) {
	return str.toLowerCase().replace(/(^|\s)(\w)/g, function (x) {
		return x.toUpperCase();
	});
}

function convertCapitalize(string) {
	return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function validateSort(sort, defaultSort) {
	return sort ? sort : defaultSort;
}

function validateOrder(order, defaultOrder) {
	if (!order) return defaultOrder;
	if (!["ASC", "DESC"].includes(order)) return defaultOrder;
	return order;
}

function validateNumber(num, defaultNum) {
	if (!num) return defaultNum;
	if (!parseInt(num) || parseInt(num) < 0) return defaultNum;
	return parseInt(num);
}

function getUniqueCode() {
	let code = (new Date().getTime()).toString(36).substr(2, 5) + Math.random().toString(36).substr(2, 5);
	return code.toUpperCase();
}

function formatData(data) {
	let result = { ...data }
	Object.keys(data).forEach(key => {
		if (typeof data[key] === 'string') {
			result[key] = data[key].trim()
		}
	})
	return result
}

function formatDataUpdate(input, fields = []) {
	const output = {}
	fields.forEach(fieldName => {
		if (typeof input[fieldName] !== 'undefined') {
			output[fieldName] = input[fieldName]
		}
	});
	return output;
}

function paymentAllowSigned(paymentMethod) {
	return [paymentMethods.CASH, paymentMethods.CP, paymentMethods.VNPTPAY].includes(`${paymentMethod || ''}`.toUpperCase())
}

function convertPhoneTo84x(str) {
	if (!str)
		return false
	if (typeof str !== 'string' && typeof str !== 'number')
		return false
	const phone = str.trim()
	if (phone.indexOf('0') === 0) {
		return phone.replace('0', 84)
	}
	return phone
}

function convertPhoneTo0x(str) {
	if (!str)
		return null
	if (typeof str !== 'string' && typeof str !== 'number')
		return null
	return `${str}`.trim().replace(/^(84|\+84|\+)/, 0)
}

function randomNumber(min, max) {
	return Math.floor(
		Math.random() * (max - min) + min
	)
}

function comparePhone(phone1, phone2) {
	if (isPhone(phone1) && isPhone(phone2)) {
		const phone84x1 = convertPhoneTo84x(phone1)
		const phone0x1 = convertPhoneTo0x(phone1)
		const phone84x2 = convertPhoneTo84x(phone2)
		const phone0x2 = convertPhoneTo0x(phone2)
		return phone84x1 === phone84x2 || phone0x1 === phone0x2
	}
	return false
}

module.exports = {
	Util,
	formatErrors,
	errorStringfy,
	formatVND,
	subTimestamp,
	removeMark,
	addPaging,
	getUniqueCode,
	formatData,
	getYearsOld,
	convertTitleCase,
	convertCapitalize,
	formatDataUpdate,
	paymentAllowSigned,
	convertPhoneTo84x,
	convertPhoneTo0x,
	randomNumber,
	comparePhone,
}
