'use strict'
const jwt = require('jsonwebtoken')

const jwtParser = (req, res, next) => {

  if (req.headers.authorization) {
    const token = req.headers.authorization.replace('Bearer ', '')
    const decodedData = jwt.decode(token);
    if (decodedData) {
      if (decodedData.brand) {
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET || 'inso_core_system', (err, decoded) => {
          if (err) {
            return res.status(401).json({
              message: 'Unauthorized.',
            })
          }
          req.jwtData = decoded
          next()
        })
      }
      else {
        req.jwtData = decodedData
        next()
      }
    } else {
      return res.status(401).send({
        message: 'Token invalid.',
      })
    }
  } else {
    return res.status(403).send({
      message: 'No token provided.',
    })
  }
}

module.exports = jwtParser