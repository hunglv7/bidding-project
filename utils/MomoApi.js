const getErrorMessage = require("../helpers/getErrorMessage");
const getMomoMessageError = require("../helpers/getMomoMessageError");
const MomoClient = require('../plugins/SercurityRequest/MomoClient')

class Util {
    constructor() {
        this.resultCode = null;
        this.partnerErrorCode = null;
        this.data = {}
        this.Client = new MomoClient({})
    }

    set(resultCode, partnerErrorCode, data) {
        this.resultCode = resultCode;
        this.partnerErrorCode = partnerErrorCode;
        this.data = data || {}
    }

    async send(res) {
        const message = getMomoMessageError(this.resultCode)
        const partnerMessage = getErrorMessage(this.partnerErrorCode)

        const data = {
            resultCode: this.resultCode,
            message,
            partnerErrorCode: this.partnerErrorCode,
            partnerMessage,
            ...this.data
        }
        const encryptData = await this.Client.encryptData(data)
        // const encryptData = data;
        return res.status(200).send(encryptData)
    }

    async getRequestBody(body) {
        return await this.Client.decryptData(body)
    }
}

module.exports = Util