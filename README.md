# BiddingService

# Organization service

*Requirement*

- `docker`, `docker-compose`

*Installation*

`cp .env.local .env`

*Running*

`docker-compose up -f docker-compose.yml`

- port: default `3004`