'use strict'

const axios = require('axios')
const FormData = require('form-data')
const dotenv = require('dotenv')
dotenv.config({ path: '.env' })

module.exports = function (filePath) {
  var formData = new FormData();
  formData.append('file', filePath.data, { filename: filePath.name })
  formData.append('contentType', filePath.mimetype)
  return axios({
    method: "post",
    url: `${process.env.API_URL}/api/storage/v1/uploads`,
    data: formData,
    headers: { 'Content-Type': `multipart/form-data; boundary=${formData._boundary}` },
  })
    .then(res => {
      if (res.status >= 200 && res.status < 300) {
        return res.data.data
      }
      return null
    })
    .catch(err => {
      console.log(`Catch Upload: ${err}`);
      return null
    })
}