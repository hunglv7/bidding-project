
module.exports = {
    getStandardResponse
}

function getStandardResponse(errorCode, errorMessage, result) {
    return {
        errorCode,
        errorMessage,
        result: result || {},
    };
}

