const { subTimestamp } = require("../utils/Utils")
const moment = require('moment')
const { isEmail, isUuid } = require("../utils/validate")

module.exports = {
    validateRegister: function (data) {
        // const {email, registerUnitId, BiddingDepartmentRelation} = data;
        let error = {}
        if (![1, 2, "1", "2"].includes(data.status)) {
            error.status = ['Trạng thái không hợp lệ (cho phép 1-Mới tạo, 2-Đã trình)']
        }

        if (!data.email) {
            error.email = ['Người dùng chưa đăng ký Email với hệ thống. Vui lòng đăng ký email trước khi thực hiện đăng ký hồ sơ đấu thầu.']
        }
        if (!data.email) {
            error.email = ['Email không được bỏ trống']
        }
        else if (!isEmail(data.email)) {
            error.email = ['Email không hợp lệ']
        }

        if (!data.registerUnitId) {
            error.registerUnitId = ['Đơn vị đăng kí không được bỏ trống']
        } else if (!isUuid(data.registerUnitId)) {
            error.registerUnitId = ['Đơn vị đăng kí không đúng']
        }

        if (!Array.isArray(data.BiddingDepartmentRelation)) {
            error.BiddingDepartmentRelation = ['Ban NV liên quan không hợp lệ']
        } else if (data.BiddingDepartmentRelation.length < 1) {
            error.BiddingDepartmentRelation = ['Ban NV liên quan không được bỏ trống']
        }
        if (!Array.isArray(data.Media)) {
            error.Media = ['Danh sách file đính kèm không hợp lệ']
        }
        if (!data.no) {
            error.no = ['Số TBMT không được bỏ trống']
        }

        if (!data.biddingName) {
            error.biddingName = ['Tên gói thầu không được bỏ trống']
        }

        if (!data.investorName) {
            error.investorName = ['Tên chủ đầu tư không được bỏ trống']
        }

        if (!data.bidSolicitor) {
            error.bidSolicitor = ['Bên mời thầu không được bỏ trống']
        }

        if (!data.address) {
            error.address = ['Địa điểm thực hiện không được bỏ trống']
        }

        // VuongTS - phase 1 - Bỏ tạm require phần dự án, phase sau thì mở nó lên - 20211209
        // if (!data.projectId) {
        //     error.projectId = ['Tên dự án/tên dự toán không được bỏ trống']
        // } else if (!isUuid(data.projectId)) {
        //     error.projectId = ['Tên dự án/tên dự toán không đúng']
        // }

        if (!data.price) {
            error.price = ['Dự toán không được bỏ trống']
        }
        else if (!Number(data.price)) {
            error.price = ['Dự toán không đúng định dạng']
        }

        if (!data.biddingForm) {
            error.biddingForm = ['Dự toán không được bỏ trống']
        }

        if (!data.selectionForm) {
            error.selectionForm = ['Hình thức LCNT không được bỏ trống']
        }

        if (!data.channelId) {
            error.channelId = ['Kênh không được bỏ trống']
        } else if (!isUuid(data.channelId)) {
            error.channelId = ['Kênh không đúng']
        }

        // if (!data.channelDetailId) {
        //     error.channelDetailId = ['Mã kênh chi tiết không được bỏ trống']
        // }

        if (!data.insuranceTypeId) {
            error.insuranceTypeId = ['Loại hình bảo hiểm không được bỏ trống']
        } else if (!isUuid(data.insuranceTypeId)) {
            error.insuranceTypeId = ['Loại hình bảo hiểm không đúng']
        }

        if (!data.capitalSource) {
            error.capitalSource = ['Nguồn vốn không được bỏ trống']
        }

        if (!data.startTime) {
            error.startTime = ['Thời gian phát hành HSMT không được bỏ trống']
        }
        else if (!moment(data.startTime, 'DD/MM/YYYY', true).isValid()) {
            error.startTime = ['Định dạng Thời gian phát hành HSMT không hợp lệ (cho phép DD/MM/YYYY)']
        }
        else if (subTimestamp(moment(data.startTime, 'DD/MM/YYYY'), moment(), 'days') < 0) {
            error.startTime = ['Thời gian phát hành HSMT phải lớn hơn hoặc bằng ngày hiện tại']
        }

        if (!data.openTime) {
            error.openTime = ['Thời điểm mở thầu không được bỏ trống']
        }
        else if (!moment(data.openTime, 'DD/MM/YYYY', true).isValid()) {
            error.openTime = ['Định dạng thời điểm mở thầu không hợp lệ (cho phép DD/MM/YYYY)']
        }
        else if (subTimestamp(moment(data.openTime, 'DD/MM/YYYY'), moment(), 'days') < 0) {
            error.openTime = ['Thời điểm mở thầu phải lớn hơn hoặc bằng ngày hiện tại']
        }

        if (!data.closeTime) {
            error.closeTime = ['Thời điểm đóng thầu không được bỏ trống']
        }
        else if (!moment(data.closeTime, 'DD/MM/YYYY', true).isValid()) {
            error.closeTime = ['Định dạng Thời điểm đóng thầu không hợp lệ (cho phép DD/MM/YYYY)']
        }
        else if (subTimestamp(moment(data.closeTime, 'DD/MM/YYYY'), moment(), 'days') < 0) {
            error.closeTime = ['Thời điểm đóng thầu phải lớn hơn hoặc bằng ngày hiện tại']
        }

        if (!data.performPlan) {
            error.performPlan = ['Phương án thực hiện không được bỏ trống']
        }

        if (!data.competitor) {
            error.competitor = ['Đối thủ cạnh tranh không được bỏ trống']
        }
        return error
    }
}