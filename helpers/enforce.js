var jwt = require('jsonwebtoken');
var axios = require('axios');
const dotenv = require('dotenv')
dotenv.config({ path: '.env' })

module.exports = function (permission = '', product = '') {

  return async (request, response, next) => {
    try {
      // TODO AUTH
      const secretKey = request.headers.internal_secret_key
      if (secretKey && secretKey === process.env.INTERNAL_SECRET_KEY) {
        request.jwtData = { isInternal: true, isSysAdmin: true, id: '766524e0-576b-11ec-a67b-03af6cf1fc6a' }
        return next()
      }

      var token = request.headers.authorization || ''
      token = token.replace('Bearer ', '')

      var decoded = jwt.verify(token, process.env.TOKEN_SECRET);

      if (decoded && decoded.realm_access && decoded.realm_access.roles) {
        const isSys = decoded.realm_access.roles.filter(role => ['SYS-ADMIN', 'SYS-OP'].includes(role))
        if (isSys.length) {
          request.jwtData = decoded
          request.jwtData.isSysAdmin = true
          return next();
        }
      }

      if (!permission) {
        throw new Error('error')
      }

      if (decoded && decoded.sub && decoded.organizationId) {
        request.jwtData = decoded
        const permRes = await axios.get(`${process.env.API_URL}/api/permission/v2/user-permissions/${decoded.sub}?permission=${permission}&group=MODULE_CONTRACT&org=${decoded.organizationId}&channel=${decoded.channelId}&isadmin=${decoded.isAdmin ? 1 : 0}`)
        const { data } = permRes.data
        if (!data.isOrgAdmin) {
          if (data.members && data.members.length) {
            request.jwtData.createdBy = [...data.members, decoded.sub]
          }
          else {
            request.jwtData.createdBy = decoded.sub
          }
        }
        return next();
      }
      throw new Error('error')
    }
    catch (error) {
      return response.status(403).json({ message: "Bạn không có quyền thực hiện thao tác này" });
    }
  }
}