'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, type) => {
  class BiddingRegister extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
        BiddingRegister.hasMany(models.bidding_department_relations, {
            foreignKey: 'registerId',
            sourceKey: 'id'
        });
    }
  };
  BiddingRegister.init({
    id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: type.UUIDV4,
        type: type.UUID
    },
    email: {
        type: type.STRING,
        allowNull: false,
        comment: 'Email đăng ký',
    },
    registerUnitId: {
        type: type.UUID,
        allowNull: false,
        comment: 'Mã Đơn vị đăng ký',
    },
    status: {
        type: type.STRING,
        allowNull: true,
        comment: 'Trạng thái hợp đồng: moi-tao, dang-trinh, da-duyet, , tu-choi, da-xoa',
    },
    no: {
        type: type.STRING,
        comment: 'Số TBMT',
    },
    investorName: {
        type: type.STRING,
        allowNull: false,
        comment: 'Tên chủ đầu tư',
    },
    bidSolicitor: {
        type: type.STRING,
        allowNull: false,
        comment: 'Bên mời thầu',
    },
    biddingName: {
        type: type.STRING,
        allowNull: false,
        comment: 'Tên gói thầu',
    },
    address: {
        type: type.STRING,
        allowNull: false,
        comment: 'Địa điểm thực hiện',
    },
    projectId: {
        type: type.UUID,
        allowNull: false,
        comment: 'ID của dự án',
    },
    price: {
        type: type.BIGINT,
        allowNull: false,
        comment: 'Dự toán gói thầu',
    },
    biddingForm: {
        type: type.STRING,
        allowNull: false,
        comment: 'Hình thức dự thầu',
    },
    selectionForm: {
        type: type.STRING,
        allowNull: false,
        comment: 'Hình thức LCNT',
    },
    channelId: {
        type: type.STRING,
        allowNull: false,
        comment: 'Kênh',
    },
    channelDetailId: {
        type: type.UUID,
        allowNull: true,
        comment: 'Mã kênh chi tiết',
    },
    insuranceTypeId: {
        type: type.STRING,
        allowNull: false,
        comment: 'Loại hình bảo hiểm',
    },
    capitalSource: {
        type: type.STRING,
        allowNull: false,
        comment: 'Nguồn vốn',
    },
    startTime: {
        allowNull: false,
        type: type.DATEONLY,
        comment: 'Thời gian phát hành HSMT',
    },
    openTime: {
        allowNull: false,
        type: type.DATEONLY,
        comment: 'Thời gian mỏ thầu',
    },
    closeTime: {
        allowNull: false,
        type: type.DATEONLY,
        comment: 'Thời gian đóng thầu',
    },
    performPlan: {
        type: type.STRING,
        allowNull: false,
        comment: 'Phương án đơn vị thực hiện',
    },
    note: {
        type: type.TEXT,
        allowNull: true,
        comment: 'Ghi chú',
    },
    competitor: {
        type: type.TEXT,
        allowNull: true,
        comment: 'Đối thủ cạnh tranh',
    },
    createdBy: {
        type: type.STRING,
        allowNull: false,
        comment: 'mã user đăng kí',
    },
    createdAt: {
        allowNull: false,
        type: type.DATE
    },
    updatedAt: {
        allowNull: false,
        type: type.DATE
    }
  }, {
    sequelize,
    modelName: 'bidding_registers',
  });
  return BiddingRegister;
};

