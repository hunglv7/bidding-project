'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, type) => {
  class BiddingFile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of type lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  BiddingFile.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: type.INTEGER
    },
    status: {
        type: type.STRING(2),
        allowNull: true,
        default: 'Y',
        comment: 'Trạng thái',
    },
    url: {
      type: type.STRING,
      comment: 'Đường dẫn ảnh',
    },
    filename: {
      type: type.STRING,
      allowNull: true,
      comment: 'Tên ảnh',
    },
    thumbnailUrl: {
      type: type.STRING,
      allowNull: true,
      comment: 'thumbnail',
    },
    size: {
      type: type.INTEGER,
      allowNull: true,
      comment: 'kích thước file',
    },
    contentType: {
      type: type.STRING,
      allowNull: true,
      comment: 'Định dạng file',
    },
    registerId: {
        type: type.UUID,
        allowNull: false,
        comment: 'mã hồ sơ đăng kí',
    },
    itemName: {
        type: type.INTEGER,
        allowNull: true,
        comment: 'Loại file: Đính kèm hay mẫu giấy ủy quyền: 1, 2',
    },
    createdAt: {
      allowNull: false,
      type: type.DATE
    },
    updatedAt: {
      allowNull: false,
      type: type.DATE
    }
  }, {
    sequelize,
    modelName: 'bidding_files',
  });
  return BiddingFile;
};