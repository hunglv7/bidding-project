'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, type) => {
  class BiddingDepartmentRelation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  BiddingDepartmentRelation.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: type.INTEGER
    },
    unitId: {
        type: type.UUID,
        allowNull: false,
        comment: 'Mã phòng ban VB liên quan',
    },
    registerId: {
        type: type.UUID,
        allowNull: false,
        comment: 'mã hồ sơ đăng kí',
    },
    createdAt: {
      allowNull: false,
      type: type.DATE
    },
    updatedAt: {
      allowNull: false,
      type: type.DATE
    }
  }, {
    sequelize,
    modelName: 'bidding_department_relations',
  });
  return BiddingDepartmentRelation;
};