const dotenv = require('dotenv')
dotenv.config({ path: '.env' })
const { formatErrors, Util, formatData } = require('../utils/Utils')
const util = new Util()
// const BiddingService = require('../services/BiddingService')
const { validateRegister } = require('../helpers/validateInput')
const ClaimBiddingService = require('../services/ClaimBiddingService')
const axios = require('axios')
const db = require('../sequelize')
const Op = require('sequelize').Op
// const dotenv = require('dotenv')
const moment = require('moment')
const momentTz = require('moment-timezone')
const UploadFileBuffer = require('../helpers/uploadFileBuffer')
// dotenv.config({ path: '.env' })

const {
    API_CODE_SUCCESS,
    API_TEXT_SUCCESS,

} = require("../helpers/constants");
const { getStandardResponse } = require("../helpers/func");

const createBidding = async (req, res) => {
    try {
        if (!req || !req.jwtData) {
            throw new Error('Authorization Exception');
        }
        const data = formatData(req.body), jwtData = req.jwtData
        const Media = req.files || {}
        data.Media = Media.Media || []
        data.email = jwtData.email;
        const contractErrors = validateRegister(data)
        if (Object.keys(contractErrors).length > 0) {
            util.setError(400, 'Dữ liệu đầu vào không hợp lệ', contractErrors);
            return util.send(res);
        }
        const result = await BiddingService.create({ ...data, creator: jwtData })
        util.setSuccess(200, 'Success', { data: result })
        return util.send(res)
    } catch (error) {
        util.setError(400, formatErrors(error));
        return util.send(res);
    }
}


const findBidding = async (req, res) => {
    const currentUser = req.jwtData;

	//Find constract by PTI code and phone
    let contract = biddingName ? await ClaimBiddingService.listBiddingName({ biddingName }, currentUser) : null;
    if (contract) {
        util.setSuccess(200, 'Success', { data: contract })
        return util.send(res)
    }
    
    if (biddingName) {
        util.setError(404, 'Không tìm thấy hợp đồng')
        return util.send(res)
    }







    // let data = await db.bidding_registers.findAll({})
    // let result = {
    //     data:data
    // }
    // res.status(200).json(result)
	// // try {
	// // 	let params = req.query

	// // 	let { count, rows } = await BiddingService.searchDidding(params);
     
	// // 	return res.status(200).json({

    // //         "a":"dá",
    // //         message: 'Success',
	// // 		totalLength: count,
	// // 		data: rows
    // //     });
	// // }
	// // catch (e) {
	// // 	console.log(e)
	// // 	util.setError(400, formatErrors(e));
	// // 	return util.json(res);
	// // }
}




// 


// const getListStocksSchema = async (req, res, next) => {
//     const schema = Joi.object({
//         size: Joi.number().required(),
//         page: Joi.number().required(),
//         txtSearch: Joi.string().allow(null, ""),
//         status: Joi.number()
//             .valid(STATUS_COMMON.ACTIVE, STATUS_COMMON.INACTIVE)
//             .empty(null),
//     });
//     validateRequest(req, next, schema);
// }


function getListBidding(req, res, next) {
         
        .getListBidding(req.body)
        .then((result) =>
            res.json(
                getStandardResponse(API_CODE_SUCCESS, API_TEXT_SUCCESS, result)
            )
        )
        .catch(next);
}




module.exports = {
    createBidding,
    getListBidding
    
}
