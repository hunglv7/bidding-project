'use strict'

const LoggingService = require('../services/LoggingService');

const afterResponse = (req, res, next) => {
    try {
        if (req.method === 'OPTIONS') {
            return next();
        }

        const { method, headers, originalUrl, body } = req;
        let { statusCode, responseData } = res;

        if(typeof responseData === 'string'){
            responseData = {
                message: responseData
            };
        }
        if(!responseData){
            responseData = {};
        }

        LoggingService.InsoApiLogging(originalUrl, method, body, headers, statusCode, responseData);
        return next();
    } catch (e) {
        return next();
    }
};

module.exports = afterResponse;
