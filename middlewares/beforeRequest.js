'use strict'

var jwt = require('jsonwebtoken');

const requestio = (req, res, next) => { 
    try {
        var token = req.headers.authorization || ''
        token = token.replace('Bearer ', '')
        if (token) {
            var decoded = jwt.verify(token, process.env.TOKEN_SECRET);
            req.jwtData = decoded;
        }
        return next();
    } catch (e) {
        return next();
    }
}

const logRequest = (req, res, next) => {
    try {
        if (req.method === 'OPTIONS') {
            return next();
        }
        const { method, headers, originalUrl, body } = req;
        console.log(originalUrl, method, body, headers);

        return next();
    } catch (e) {
        return next();
    }
};

module.exports = {
    logRequest,
    requestio,
}
