const axios = require('axios')
const db = require('../sequelize')
const Op = require('sequelize').Op
const dotenv = require('dotenv')
const moment = require('moment')
const momentTz = require('moment-timezone')
const UploadFileBuffer = require('../helpers/uploadFileBuffer')
dotenv.config({ path: '.env' })

module.exports = {
    create: async function (data) {
        const t = await db.sequelize.transaction();
        try {
            let contractCreated
            let contractData = data
            console.log(' test', moment(`${data.startTime}T00:01:00+07:00`).format('YYYY-MM-DD'));
            console.log(' test2', moment(data.startTime, 'DD/MM/YYYY').format('YYYY-MM-DD'));
            contractData = {
                ...data,
                ... {
                    createdBy: data.creator.preferred_username,
                    startTime: moment(data.startTime, 'DD/MM/YYYY').format('YYYY-MM-DD'),
                    openTime: moment(data.openTime, 'DD/MM/YYYY').format('YYYY-MM-DD'),
                    closeTime: moment(data.closeTime, 'DD/MM/YYYY').format('YYYY-MM-DD'),
                }
            }
            delete contractData.id
            contractCreated = await db.bidding_registers.create(contractData, { transaction: t })
            
            const registerUnitRelation = await Promise.all(data.BiddingDepartmentRelation.map(pkg => {
                const relation = {
                    unitId: pkg,
                    registerId: contractCreated.id,
                }
                return db.bidding_department_relations.create(relation, { transaction: t })
            }))

            const media = await Promise.all(Object.keys(data.Media).map(async key => {
                const filePath = data.Media[key]
                if (filePath) {
                    const source = await UploadFileBuffer(filePath)
                    await db.bidding_files.create({
                        filename: filePath.name,
                        itemName: 1, // File dinh kem
                        url: source,
                        registerId: contractCreated.id,
                    })
                }
            }))

            await t.commit()

            return { contractId: contractCreated.id }
        } catch (error) {
            await t.rollback()
            throw error;
        }
    },
    //

     searchDidding: async function(data) {
        try {
            // let { biddingName, registerUnitId,limit = 10, offset = 0 } = data;
            // console.log(data)
            // let condition = {};
            // // condition['$contract.code$'] = { [Op.not]: null }
            // // condition['$contract.status$'] = contractStatusNew.SIGNED
            
            // if (!!biddingName) {
            //     condition.biddingName = data.biddingName
            // }
          
            // if (!!registerUnitId) {
            //     condition.registerUnitId = data.registerUnitId
            // }
        
            console.log(biddingName);
            return biddingName;
    
        } catch (error) {
            console.log(error)
            throw error
        }
    },


    // searchDidding: async function(data) {
    //     try {
    //         let {  biddingName,  orderBy, limit = 10, offset = 0 } = data;
    //         let order = [];
    //         if (!orderBy || (orderBy && orderBy.length < 1)) {
    //             order = [
    //                 ['updatedAt', 'DESC']
    //             ]
    //         } else {
    //             order = [
    //                 ['code', 'DESC']
    //             ]
    //         }
    //         let condition = {};
    //         condition['$contract.code$'] = { [Op.not]: null }
    //         condition['$contract.status$'] = contractStatusNew.SIGNED
    //         if (!!firstName) {
    //             condition.firstName = data.firstName
    //         }
    //         if (!!lastName) {
    //             condition.lastName = data.lastName
    //         }
    //         if (!!flightNumber) {
    //             condition = {
    //                 ...condition,
    //                 [Op.and]: [
    //                     {
    //                         [Op.or]: [
    //                             { flightNumber: data.flightNumber },
    //                             {
    //                                 transitFlightNumbers: {
    //                                     [Op.iLike]: `%"${data.flightNumber}"%`
    //                                 }
    //                             }
    //                         ]
    //                     }
    //                 ]
    //             }
    //         }
    //         if (!!customerId) {
    //             condition.customerId = data.customerId
    //         }
    //         if (bookingNumber) {
    //             condition = {
    //                 ...condition,
    //                 [Op.or]: [
    //                     { bookingNumber: data.bookingNumber },
    //                     { bookingCode: data.bookingNumber }
    //                 ]
    //             }
    //         }
    //         return await db.bidding_registers.findAndCountAll({
    //             where: condition,
    //             subQuery: false,
    //             order,
    //             limit,
    //             offset,
    //         })
    
    //     } catch (error) {
    //         console.log(error)
    //         throw error
    //     }
    // }
    

}
