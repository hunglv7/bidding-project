
const db = require('../sequelize')
const {} = require("../helpers/constants");

async function listBiddingName({ biddingName }, user) {
    // let { channelId, organizationId } = await getChannelInfo(user);
    // code = code.toString().replace(/\s/g, '');

    // const isAmendmentCode = isAmendmentContractCode(code);
    // const phone0x = convertPhoneTo0x(phone);
    // const phone84x = convertPhoneTo84x(phone);
    // const contractCode = isAmendmentCode ? code.replace(/^[0-9]{2}SDBS/, '') : code;
    //Tra cuu uu tien tim so DT da SDBS truoc
    // let dataEditing = await getContractsEditingByParams({ phone, contractCode, amendmentCode: isAmendmentCode ? code : null });
    // if (dataEditing) {
    //     let dataOutput = [];
    //     for (let i = 0; i < dataEditing.length; ++i) {
    //         if (dataEditing[i].contractCode != contractCode) {
    //             continue;
    //         }
    //         //Case match 100%
    //         if ([phone0x, phone84x].includes(dataEditing[i].changeData.buyerPhone)) {
    //             let dataItem = await getContractsByParams({ code: dataEditing[i].contractCode, channelId, organizationId, offset: 0, limit: 1, needDetails: false });
    //             if (dataItem && dataItem.rows && dataItem.rows.length > 0) {
    //                 // Lay item dau tien thoi
    //                 dataOutput.push(dataItem.rows[0]);
    //             }
    //         }
    //     }
    //     if (dataOutput.length > 0 && (offset * limit) < dataOutput.length) {
    //         let data = {
    //             rows: dataOutput.slice(offset * limit, (offset + 1) * limit), // paging
    //             count: dataOutput.length
    //         };
    //         return await checkSignatureImage(data);
    //     } else if (dataOutput.length > 0) {
    //         //out of range - return null
    //         return null;
    //     }
    // }

    // if (!isContractCode(code)) {
    //     return null
    // }

    // let data = {
    //     rows: null,
    //     count: 0
    // }

    // Tra cuu theo ma HD & phone chua SDBS
    let datas = await getAllContractsByParams({ biddingName});
    if (datas && dataMatch100Percent.length > 0) {
        for (let i = 0; i < dataMatch100Percent.length; ++i) {
            const item = dataMatch100Percent[i];
            if (data.count > 0) {
                if (data.rows.find(x => x.code === item.code)) {
                    //Bo qua cac hop dong da tim thay tu truoc
                    continue;
                }
            }

            if (await hasEdittingChange(item.id, { phone, amendmentCode: isAmendmentCode ? code : null })) {
                continue;
            }
            if (!data.rows) {
                data.rows = [];
                data.count = 0;
            }

            data.rows.push(item);
            ++data.count;
        }
    }
    //paging
    data.rows = data.rows && (offset * limit) < data.rows.length ? data.rows.slice(offset * limit, (offset + 1) * limit) : null;
    return await checkSignatureImage(data);
}


///




async function listBiddingName({ email, phone, code, taxCode, identityNumber, referNumber, channelId, organizationId, offset, limit, serial, needDetails = true }) {
    let where = {
        isDeleted: false,
        status: contractStatusNew.SIGNED,
    };
    let include = [
        {
            model: db.buyer,
            attributes: ['fullName', 'phone', 'email']
        },
        {
            model: db.insured_customer,
            attributes: ['fullName', 'phone', 'email']
        },
        {
            model: db.contract_car,
            attributes: ['licenseNumber', 'chassisNumber', 'machineNumber', 'ownerFullName']
        },
        {
            model: db.contract_motobike,
            attributes: ['licenseNumber', 'chassisNumber', 'machineNumber', 'ownerFullName']
        },
        {
            model: db.contract_electric,
            attributes: ['serial', 'madeBy', 'model']
        }
    ];

    if (channelId) {
        where = Object.assign(where, { channelId });
    }
    if (organizationId) {
        where = Object.assign(where, { organizationId });
    }
    if (code) {
        where = Object.assign(where, { code });
    }
    if (phone) {
        const phone0x = convertPhoneTo0x(phone);
        const phone84x = convertPhoneTo84x(phone);
        if (where[Op.and]) {
            where[Op.and].push({
                [Op.or]: [
                    { '$buyer.phone$': phone0x },
                    { '$buyer.phone$': phone84x },
                    { '$insured_customer.phone$': phone0x },
                    { '$insured_customer.phone$': phone84x },
                ]
            });
        } else {
            where[Op.and] = [
                {
                    [Op.or]: [
                        { '$buyer.phone$': phone0x },
                        { '$buyer.phone$': phone84x },
                        { '$insured_customer.phone$': phone0x },
                        { '$insured_customer.phone$': phone84x },
                    ]
                }
            ];
        }
    }
    if (email) {
        if (where[Op.and]) {
            where[Op.and].push({
                [Op.or]: [
                    { companyEmail: email },
                    { '$buyer.email$': email },
                    { '$insured_customer.email$': email },
                ]
            });
        } else {
            where[Op.and] = [
                {
                    [Op.or]: [
                        { companyEmail: email },
                        { '$buyer.email$': email },
                        { '$insured_customer.email$': email },
                    ]
                }
            ];
        }
    }
    if (taxCode) {
        where = Object.assign(where, { companyTaxCode: taxCode });
    }
    if (identityNumber) {
        if (where[Op.and]) {
            where[Op.and].push({
                [Op.or]: [
                    { '$insured_customer.cardId$': identityNumber },
                    { '$buyer.cardId$': identityNumber },
                ]
            });
        } else {
            where[Op.and] = [
                {
                    [Op.or]: [
                        { '$insured_customer.cardId$': identityNumber },
                        { '$buyer.cardId$': identityNumber },
                    ]
                }
            ];
        }
    }
    if (referNumber) {
        where = Object.assign(where, { referNumber });
    }
    if (serial) {
        where = Object.assign(where, { '$contract_electric.serial$': serial });
    }

    let data = await db.contract.findAndCountAll({
        where,
        include,
        offset,
        limit,
        attributes: ['id', 'code', 'objectNumber', 'effectiveAt', 'expiredAt', 'signatureImage', 'status']
    });
    return needDetails ? await checkSignatureImage(data) : data;
}

//
async function getListBidding(params) {
    const { limit, offset } = getPagination(params.page, params.size);

    let whereObj = {};

    if (params.status !== undefined) {
        whereObj = { ...whereObj, status: params.status };
    }

    if (params.txtSearch) {
        whereObj = {
            ...whereObj,
            [Op.or]: [
                {
                    code: {
                        [Op.like]: `%${params.txtSearch}%`,
                    },
                },
                {
                    name: {
                        [Op.like]: `%${params.txtSearch}%`,
                    },
                },
            ],
        };
    }

    const listData = await db.stocks.findAndCountAll({
        where: whereObj,
        limit,
        offset,
        order: [["createdAt", "DESC"]],
    });

    const result = getPagingData(listData, params.page, limit);

    return result;
}






module.exports = {
    getListBidding
}